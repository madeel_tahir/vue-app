# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Header>` | `<header>` (components/Header.vue)
- `<NuxtLogo>` | `<nuxt-logo>` (components/NuxtLogo.vue)
- `<Pagination>` | `<pagination>` (components/Pagination.vue)
- `<Tutorial>` | `<tutorial>` (components/Tutorial.vue)
- `<UserCard>` | `<user-card>` (components/UserCard.vue)
- `<UserFilterForm>` | `<user-filter-form>` (components/UserFilterForm.vue)
