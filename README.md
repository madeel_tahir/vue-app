# Vue App #

Vue app using Nuxt, fetches user's listing from API with search and sort filter, pagination and user registration.

### Steps ###

* Clone Repo
* cd vue-app
* npm install

### Development Server ###

* npm run dev

### Production Build ###

* npm run generate